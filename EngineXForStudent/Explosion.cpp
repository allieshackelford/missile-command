#include "Explosion.h"

// create explosion, set position and initialize starting radius
Explosion::Explosion(exEngineInterface *pEngine, exVector2 position) : GameObject(pEngine){
	mEngine = pEngine;
	mPosition = position;
	secondPos = { position.x + 10, position.y - 8 };
	radius1 = 5;
	radius2 = 2;
	timer = 0;
	multiplier = 1;
}

float Explosion::X() {
	return mPosition.x;
}

float Explosion::Y() {
	return mPosition.y;
}

// draw explosion, increase radius by multiplier each call
void Explosion::Draw(){
	radius1 = 5 * multiplier;
	mEngine->DrawCircle(mPosition, radius1, { 255, 89, 26,255 }, 0);
	if (timer >= 5) {	// start drawing secondary explosion
		mEngine->DrawCircle(secondPos, radius2 * multiplier, { 232, 42, 12,255 }, 0);
	}
	if ((int)timer % 4 == 0) {
		multiplier++;
	}
}

// update, draw explosion for duration of timer then return true
bool Explosion::Update(){
	if (timer >= 20) {
		return true;
	}

	else {
		Draw();
		timer++;
		return false;
	}
}