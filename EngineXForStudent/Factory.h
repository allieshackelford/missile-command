#pragma once
#include "City.h"
#include "Missile.h"
#include "Explosion.h"
#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"

class Factory {
public:
	// enum of all object types
	enum objTypes {
		city,
		missile,
		explosion
	};

	// factory function to create GameObject
	GameObject* MakeObject(objTypes type, exEngineInterface *engine, exVector2 position, exVector2 endPos, float lerpTime) { // endPos for missiles only
		switch (type) {
			case city:
				return new City(engine, position);
				break;
			case missile:
				return new Missile(engine, position, endPos, lerpTime);
				break;
			case explosion:
				return new Explosion(engine, position);
				break;
			default:
				break;
		}
	}
};