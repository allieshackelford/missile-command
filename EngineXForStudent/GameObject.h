#pragma once
#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"

// virtual class, parent to city, missile and explosion
class GameObject {
protected:
	GameObject(exEngineInterface* pEngine) : mEngine(pEngine){}
	exVector2 mPosition;
	exEngineInterface *mEngine;
};