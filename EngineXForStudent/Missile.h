#pragma once
#include "Explosion.h"
#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"
#include <vector>
using namespace std;

class Missile : public GameObject {
public:
	// ----- constructor --------
	Missile(exEngineInterface *pEngine, exVector2 startPos, exVector2 endPos, float lerpTime);
	// ------ update, return true if reached end position
	bool Update();
	// ----- collision checker -------
	void CheckCollision(vector<Explosion*> explosions);
	exVector2 hitPos;
	bool destroyed;
	void SetColor(exColor newColor);
private:
	bool CirclePointCol(Explosion* explosion);
	void Draw();
	// ---- lerp information ----- 
	float lerpPos(float a, float b, float t);
	exVector2 updatePos;
	float time;
	exColor color;
};