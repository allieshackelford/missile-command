#include "City.h"
#include <math.h>
using namespace std;

// create city and initialize position
City::City(exEngineInterface *pEngine, exVector2 position) : GameObject(pEngine){
	mEngine = pEngine;
	mPosition = position;
}

// draw city
void City::Draw(){
	mEngine->DrawBox(mPosition, { mPosition.x + 40, mPosition.y + 25 }, { 70,117,232,255 }, 1);
}

// update , draw city and report any collisions with enemy explosions
bool City::Update(vector<Explosion*> enemyExplosions) {
	Draw();
	if (CheckCollisions(enemyExplosions)) {
		return true;
	}
	return false;
}

// check all collisions
bool City::CheckCollisions(vector<Explosion*> explosions) {
	for (int i = 0; i < explosions.size(); i++) {
		bool hit = BoxCircleCollision(explosions[i]);
		if (hit) {
			return true;
		}
	}

	return false;
}

// box circle collision checker
bool City::BoxCircleCollision(Explosion *explosion) {
	float yDis = fabs(explosion->Y() - mPosition.y);
	if (yDis <= explosion->radius1 && explosion->X() >= mPosition.x && explosion->X() <= mPosition.x + 40){
		return true;
	}

	return false;
}