#pragma once
#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"
#include "Factory.h"
#include "Base.h"
#include "ScoreManager.h"
#include <vector>
using namespace std;

class Player {
public:
	// ----- constructor and destructor --------
	Player(exEngineInterface *pEngine, Factory *factory);
	~Player();		// DONT FORGET TO DELETE STUFF
	// -------- update -------
	bool Update(vector<Explosion*> enemyExplosions);
	// ----- check if player lost -------
	bool CheckLose(int destroyed);
	void DrawMouse(exVector2 mousePos);
	void Draw();
	float Fire(bool A, bool S, bool D, exVector2 firePos);
	// ----- PLAYER OBJECTS ----
	vector<Missile*> missiles;
	vector<Explosion*> explosions;
	vector<exVector2> positions;
	vector<Base*> bases;
	vector<City*> cities;
private:
	exEngineInterface *mEngine;
	Factory	*mFactory;
};