#pragma once
#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"
#include "Explosion.h"
#include <vector>
using namespace std;

class Base : public GameObject{
public:
	// ----- Constructor ---------
	Base(exEngineInterface *pEngine, exVector2 basePos);
	// ------ Draw -----------
	void Draw();
	void Dead();
	// ------Update and Collision Checks --------
	void Update(vector<Explosion*> enemyExplosions);
	void CheckCollisions(vector<Explosion*> explosions);
	bool CircleCollision(Explosion *explosion);
	exVector2 getPosition();
	bool destroyed;
};