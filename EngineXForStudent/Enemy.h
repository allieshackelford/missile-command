#pragma once
#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"
#include "Factory.h"
#include "ScoreManager.h"
#include <vector>
using namespace std;

class Enemy {
public:
	// ------ constructor ------
	Enemy(exEngineInterface *pEngine, Factory *factory);
	// ------- update --------
	void Update(vector<exVector2> positions, vector<Explosion*> explosions, ScoreManager *score);
	// ------ Fire and missile/explosions ------
	void Fire(exVector2 target);
	vector<Missile*> missiles;
	vector<Explosion*> explosions;
private:
	float timer;	// keep track of time in between attacks
	Factory *mFactory;
	exEngineInterface *mEngine;
};