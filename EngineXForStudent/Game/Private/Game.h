//
// * ENGINE-X
// * SAMPLE GAME
//
// + Game.h
// definition of MyGame, an implementation of exGameInterface
//

#include "Game/Public/GameInterface.h"
#include "Engine/Public/EngineTypes.h"

#include "Player.h"
#include "Enemy.h"
#include "Factory.h"
#include "ScoreManager.h"

//-----------------------------------------------------------------
//-----------------------------------------------------------------

class MyGame : public exGameInterface
{
public:

								MyGame();
	virtual						~MyGame();

	virtual void				Initialize( exEngineInterface* pEngine );

	virtual const char*			GetWindowName() const;
	virtual void				GetClearColor( exColor& color ) const;

	virtual void				OnEvent( SDL_Event* pEvent );
	virtual void				OnEventsConsumed();

	virtual void				Run( float fDeltaT );

private:

	exEngineInterface*			mEngine;

	int							mFontID;
	int							mFont2;

	bool						A;
	bool						S;
	bool						D;
	bool						Enter;
	
	bool						gameStarted;

	float						fireTimer;

	exVector2					mousePos;
	Player						*player;
	Enemy						*enemy;
	Factory						*factory;
	ScoreManager				*score;

	exVector2					mTextPosition;

};
