//
// * ENGINE-X
// * SAMPLE GAME
//
// + Game.cpp
// implementation of MyGame, an implementation of exGameInterface
//

#include "Game/Private/Game.h"

#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"

//-----------------------------------------------------------------
//-----------------------------------------------------------------

const char* gWindowName = "Sample EngineX Game";

//-----------------------------------------------------------------
//-----------------------------------------------------------------

MyGame::MyGame()
	: mEngine( nullptr )
	, mFontID( -1 )
	, A( false )
	, S( false )
	, D(false)
	, fireTimer(0)
	, gameStarted(false)
	, Enter(false)
{
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

MyGame::~MyGame()
{
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::Initialize( exEngineInterface* pEngine )
{
	mEngine = pEngine;

	mFontID = mEngine->LoadFont( "Kasattack-Regular.ttf", 32 );
	mFont2 = mEngine->LoadFont("Kasattack-Regular.ttf", 20);

	factory = new Factory();
	player = new Player(mEngine, factory);
	enemy = new Enemy(mEngine, factory);
	score = new ScoreManager(mEngine);
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

const char* MyGame::GetWindowName() const
{
	return gWindowName;
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::GetClearColor( exColor& color ) const
{
	color.mColor[0] = 0;
	color.mColor[1] = 0;
	color.mColor[2] = 0;
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::OnEvent( SDL_Event* pEvent )
{
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::OnEventsConsumed()
{
	int nKeys = 0;
	const Uint8 *pState = SDL_GetKeyboardState( &nKeys );

	A = pState[SDL_SCANCODE_A];
	S = pState[SDL_SCANCODE_S];
	D = pState[SDL_SCANCODE_D];
	Enter = pState[SDL_SCANCODE_RETURN];

	int mouseX, mouseY;
	SDL_GetMouseState(&mouseX, &mouseY);
	mousePos = { (float)mouseX, (float)mouseY };	// exVector2
}

//-----------------------------------------------------------------
//-----------------------------------------------------------------

void MyGame::Run( float fDeltaT )
{
	if (!gameStarted) {	// show menu
		if (score->Menu(Enter, mFontID, mFont2)) {
			gameStarted = true;
		}
	}
	else {	// play game
		score->Update(mFontID);
		if (fireTimer == 0 && (A || S || D)){
			fireTimer = player->Fire(A, S, D, mousePos);
		}
		else {
			if (fireTimer > 0) {
				fireTimer -= 1;
			}
			else {
				fireTimer = 0;
			}
		}
		player->DrawMouse(mousePos);
		if (player->Update(enemy->explosions)) {
			gameStarted = false;
			player->bases.clear();
			player->cities.clear();
		}
		// take in positions of bases and cities to fire at
		// take in player missile explosions to check against enemy missiles
		enemy->Update(player->positions, player->explosions, score);
	}

}
