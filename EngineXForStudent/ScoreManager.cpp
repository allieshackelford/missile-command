#include "ScoreManager.h"
#include <string>

// create score 
ScoreManager::ScoreManager(exEngineInterface* pEngine) {
	mEngine = pEngine;
	score = 0;
}

// draw score to top left corner
void ScoreManager::Update(int fontID) {
	mEngine->DrawText(fontID, { 10,10 }, "Score: ", { 255,255,255,255 }, 0);
	mEngine->DrawText(fontID, { 120,10 }, to_string(score).c_str(), { 255,255,255,255 }, 0);
}

// add point to score
void ScoreManager::AddPoint(int sc1) {
	score += sc1;
}


// draw menu, until user presses ENTER
bool ScoreManager::Menu(bool enter, int bigFont, int smallFont) {
	mEngine->DrawText(bigFont, { 250,200 }, "MISSILE COMMAND", { 255,255,255,255 }, 0);
	mEngine->DrawBox({ 300, 350 }, { 500, 400 }, { 1,20,235,255 }, 1);
	mEngine->DrawText(bigFont, { 350, 360 }, "PLAY", {255,255,255,255},0);
	mEngine->DrawText(smallFont, { 300, 500 }, "press enter to play...", { 173,232,12,150 }, 0);

	if (enter) {
		return true;
	}

	return false;
}