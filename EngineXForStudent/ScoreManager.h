#pragma once
#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"
#include "Player.h"
class exEngineInterface;

class ScoreManager {
public:
	ScoreManager(exEngineInterface* pEngine);
	// --- UPDATE SCORE------
	void Update(int fontID);
	void AddPoint(int sc1);
	bool Menu(bool enter, int fontID,int smallFont);

private:
	int score;
	exEngineInterface* mEngine;
};