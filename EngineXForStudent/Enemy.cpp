#include "Enemy.h"
#include <stdlib.h>

// create enemy and initialize fire timer
Enemy::Enemy(exEngineInterface *pEngine, Factory *factory){
	mEngine = pEngine;
	mFactory = factory;
	timer = 0;
}

// update, if timer at zero fire another missile
void Enemy::Update(vector<exVector2> positions, vector<Explosion*> playerExplosions, ScoreManager *score) {
	if (timer == 0) {	// if timer 0, fire missile
		int targetIndex = rand() % positions.size();	// choose random player target
		Fire(positions[targetIndex]);
		timer = 50;	// set countdown to 50 frames
	}
	else {	// countdown fire timer
		timer--;
	}

	for (int i = 0; i < missiles.size(); i++) {	// iterate through missiles and update
		bool done = missiles[i]->Update();
		missiles[i]->CheckCollision(playerExplosions);	// check for collision with player explosions
		if (done) {
			//create explosion
			explosions.push_back((Explosion*)mFactory->MakeObject(Factory::objTypes::explosion, mEngine, missiles[i]->hitPos, { 0,0 }, 0));
			missiles.erase(missiles.begin() + i);	// erase missile from vector
		}
		else if (missiles[i]->destroyed) {	// if missile destroyed
			score->AddPoint(10);	// add 10 to score
			missiles.erase(missiles.begin() + i);
		}
	}

	for (int i = 0; i < explosions.size(); i++) {	// iterate explosions and update
		bool done = explosions[i]->Update();
		if (done) {
			explosions.erase(explosions.begin() + i);
		}
	}
}


void Enemy::Fire(exVector2 target) {	// fire missile at target
	exVector2 randomFirePos;
	randomFirePos.x = rand() % 790;		// choose random position at top of screen
	randomFirePos.y = 0;
	missiles.push_back((Missile*)mFactory->MakeObject(Factory::objTypes::missile, mEngine, randomFirePos, target, .005));
}