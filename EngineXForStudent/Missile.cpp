#include "Missile.h"
#include <math.h>
using namespace std;

// create and set start/end position, and lerp position (different for player and enemy)
Missile::Missile(exEngineInterface *pEngine, exVector2 startPos, exVector2 endPos, float lerpTime) : GameObject(pEngine) {
	mEngine = pEngine;
	mPosition = startPos;
	updatePos = startPos;
	hitPos = endPos;
	destroyed = false;
	time = lerpTime;
	color = { 255,0,0,255 };
}

// lerp two points
float Missile::lerpPos(float a, float b, float t) {
	return (1 - t) * a + t * b;
}

// update, lerp from start to end position
bool Missile::Update(){
	updatePos.x = lerpPos(updatePos.x, hitPos.x, time);
	updatePos.y = lerpPos(updatePos.y, hitPos.y, time);

	// if close to end position, create explosion
	if ((fabs(hitPos.x - updatePos.x) < 15 && fabs(hitPos.y - updatePos.y) < 15) && !destroyed) {
		// explosion
		return true;
	}
	else {
		if (!destroyed) {
			Draw();
		}
		return false;
	}
}

void Missile::Draw(){
	// draw small white box as arrow head
	mEngine->DrawLineCircle(updatePos, 5, {255,255,255,255}, 3);
	mEngine->DrawLine(mPosition, updatePos, color, 3);
}

// check collisions against explosions
void Missile::CheckCollision(vector<Explosion*> explosions){
	bool checker = false;
	for (int i = 0; i < explosions.size(); i++) {
		checker = CirclePointCol(explosions[i]);
		if (checker) {
			destroyed = true;
		}
	}
}

// circle point collision, only check against tip of missile
bool Missile::CirclePointCol(Explosion* explosion) {
	// distance between point to explosion
	float distance = sqrt(pow(updatePos.x - explosion->X(), 2) + pow(updatePos.y - explosion->Y(), 2));

	if (distance <= explosion->radius1) {
		return true;
	}

	return false;
}

// change missile line color
void Missile::SetColor(exColor newColor) {
	color = newColor;
}