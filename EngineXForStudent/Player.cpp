#include "Player.h"
#include <iostream>
using namespace std;

// create player and initialize bases and cities positions
Player::Player(exEngineInterface *pEngine, Factory *factory) {
	mEngine = pEngine;
	mFactory = factory;

	// took base out of factory so it can use the factory to make missiles
	bases.push_back(new Base(pEngine, { 45,560 }));		// first base
	positions.push_back({ 45,500 });
	bases.push_back(new Base(pEngine, { 400,560 }));		// second base
	positions.push_back({ 400,500 });
	bases.push_back(new Base(pEngine, { 750,560 }));		// third base
	positions.push_back({ 750,500 });

	// create cities
	cities.push_back((City*)factory->MakeObject(Factory::objTypes::city, pEngine, {115,565}, { 0,0 }, 0));
	positions.push_back({ 135,565 });
	cities.push_back((City*)factory->MakeObject(Factory::objTypes::city, pEngine, {210,565 }, { 0,0 }, 0));
	positions.push_back({ 230,565 });
	cities.push_back((City*)factory->MakeObject(Factory::objTypes::city, pEngine, {300,565 }, { 0,0 }, 0));
	positions.push_back({ 320,565 });
	cities.push_back((City*)factory->MakeObject(Factory::objTypes::city, pEngine, { 460,565 }, { 0,0 }, 0));
	positions.push_back({ 480,565 });
	cities.push_back((City*)factory->MakeObject(Factory::objTypes::city, pEngine, { 550,565 }, { 0,0 }, 0));
	positions.push_back({ 570,565 });
	cities.push_back((City*)factory->MakeObject(Factory::objTypes::city, pEngine, { 630,565 }, { 0,0 }, 0));
	positions.push_back({ 650,565 });
}

Player::~Player() {
	// delete cities and bases
}

// draw ground
void Player::Draw() {
	mEngine->DrawBox({ 0,570 }, { 800, 600 }, { 232,164,70,255 }, 1);
}

// draw crosshairs for mouse
void Player::DrawMouse(exVector2 mousePos) {
	mEngine->DrawLineCircle(mousePos, 10, { 0,101,184,255 }, 0);
	mEngine->DrawLine({ mousePos.x - 10, mousePos.y - 6 }, {mousePos.x + 10, mousePos.y + 6}, { 158, 250, 255, 255 }, 0);
	mEngine->DrawLine({ mousePos.x - 10, mousePos.y + 6 }, { mousePos.x + 10, mousePos.y - 6 }, { 158, 250, 255, 255 }, 0);
}

// huge update, update all cities, bases, missiles and explosions
bool Player::Update(vector<Explosion*> enemyExplosions) {
	int basesDes = 0;
	Draw();
	for (int i = 0; i < bases.size(); i++) {	// for each base, update itself
		bases[i]->Update(enemyExplosions);
		if (bases[i]->destroyed) {
			basesDes++;
		}
	}
	
	if (CheckLose(basesDes)) {	// check if player has lost
		return true;
	}

	// update cities
	for (int i = 0; i < cities.size(); i++) {
		if (cities[i]->Update(enemyExplosions)) {
			cities.erase(cities.begin() + i);
		}
	}

	// update misiles
	for (int i = 0; i < missiles.size(); i++) {
		missiles[i]->SetColor({ 117,255,13, 255});
		bool createExplosion = missiles[i]->Update();
		if (createExplosion) {	// if finished, create explosion
			explosions.push_back((Explosion*)mFactory->MakeObject(Factory::objTypes::explosion, mEngine, missiles[i]->hitPos, {0,0}, 0));
			missiles.erase(missiles.begin() + i);
		}
		else if (missiles[i]->destroyed) {	// if missile was destroyed, erase from vector
			missiles.erase(missiles.begin() + i);
		}
	}

	// update explosion
	for (int i = 0; i < explosions.size(); i++) {
		bool done = explosions[i]->Update();
		if (done) {
			explosions.erase(explosions.begin() + i);
		}
	}

	return false;
}

// check if all bases and cities have been destroyed
bool Player::CheckLose(int destroyed) {
	if (destroyed && cities.size() == 0) {
		return true;
	}

	return false;
}

// try to fire
float Player::Fire(bool A, bool S, bool D, exVector2 firePos){
	if (A && !bases[0]->destroyed) {	// if A and base not destroyed
		// create missile with base start position and firePos end position
		missiles.push_back((Missile*)mFactory->MakeObject(Factory::objTypes::missile, mEngine, bases[0]->getPosition(), firePos, .1));
		mEngine->DrawLine({ firePos.x - 10, firePos.y - 6 }, { firePos.x + 10, firePos.y + 6 }, { 255, 255, 255, 255 }, 0);
		mEngine->DrawLine({ firePos.x - 10, firePos.y + 6 }, { firePos.x + 10, firePos.y - 6 }, { 255, 255, 255, 255 }, 0);
		return 10;
	}

	if (S && !bases[1]->destroyed) { // if S and base not destroyed
		// create missile with base start position and firePos end position
		missiles.push_back((Missile*)mFactory->MakeObject(Factory::objTypes::missile, mEngine, bases[1]->getPosition(), firePos, .1));
		mEngine->DrawLine({ firePos.x - 10, firePos.y - 6 }, { firePos.x + 10, firePos.y + 6 }, { 255, 255, 255, 255 }, 0);
		mEngine->DrawLine({ firePos.x - 10, firePos.y + 6 }, { firePos.x + 10, firePos.y - 6 }, { 255, 255, 255, 255 }, 0);
		return 10;
	}

	if (D && !bases[2]->destroyed) { // if D and base not destroyed
		// create missile with base start position and firePos end position
		missiles.push_back((Missile*)mFactory->MakeObject(Factory::objTypes::missile, mEngine, bases[2]->getPosition(), firePos, .1));
		mEngine->DrawLine({ firePos.x - 10, firePos.y - 6 }, { firePos.x + 10, firePos.y + 6 }, { 255, 255, 255, 255 }, 0);
		mEngine->DrawLine({ firePos.x - 10, firePos.y + 6 }, { firePos.x + 10, firePos.y - 6 }, { 255, 255, 255, 255 }, 0);
		return 10;
	}
}