#pragma once

#include "Engine/Public/EngineInterface.h"
#include "Engine/Public/SDL.h"
#include "Explosion.h"
#include <vector>
using namespace std;

class City : public GameObject {
public:
	// ---- Constructor -------
	City(exEngineInterface *pEngine, exVector2 position);
	// ------ Update and Draw ------
	bool Update(vector<Explosion*> enemyExplosions);
	void Draw();
	// ----- Collision Checkers ---------
	bool CheckCollisions(vector<Explosion*> explosions);
	bool BoxCircleCollision(Explosion *explosion);
};