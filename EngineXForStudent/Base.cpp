#include "Base.h"

// create base and initialize destroyed to false
Base::Base(exEngineInterface *pEngine, exVector2 basePos) : GameObject(pEngine) {
	mEngine = pEngine;
	mPosition = basePos;
	destroyed = false;
}

// draw bases and check for collisions with enemy explosions
void Base::Update(vector<Explosion*> enemyExplosions) {
	Draw();
	CheckCollisions(enemyExplosions);
}

// if collision with explosion, change destroyed to true
void Base::CheckCollisions(vector<Explosion*> explosions) {
	for (int i = 0; i < explosions.size(); i++) {
		if (CircleCollision(explosions[i])) {
			destroyed = true;
		}
	}
}

// check circle collision, return true if collided
bool Base::CircleCollision(Explosion *explosion) {
	float distance = sqrt(pow(mPosition.x - explosion->X(), 2) + pow(mPosition.y - explosion->Y(), 2));
	if (distance < 50 + explosion->radius1) {
		return true;
	}

	return false;
}

// draw base
void Base::Draw() {
	mEngine->DrawCircle(mPosition, 50, { 255,0,0,255 }, 2);
	if (destroyed) {
		Dead();
	}
}

// if dead, draw X across base
void Base::Dead() {
	mEngine->DrawLine({ mPosition.x - 30, mPosition.y - 30 }, { mPosition.x + 30, mPosition.y + 30 }, { 0,255,55,255 }, 1);
	mEngine->DrawLine({ mPosition.x - 30, mPosition.y + 30 }, { mPosition.x + 30, mPosition.y - 30 }, { 0,255,55,255 }, 1);
}

// return position
exVector2 Base::getPosition() {
	return mPosition;
}