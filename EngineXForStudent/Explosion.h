#pragma once
#include "GameObject.h"

class Explosion : public GameObject{
public:
	// --- constructor -----
	Explosion(exEngineInterface *pEngine, exVector2 position);
	// ----- update, return true if done ------
	bool Update();
	// -----  Getters -------
	float X();
	float Y();
	float radius1;
private:
	void Draw();
	// ----- private values ------
	float radius2;
	exVector2 secondPos;
	float timer;
	float multiplier;
};